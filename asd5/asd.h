#include <iostream>

struct node {
    int key;
    struct node *right;
    struct node *left;
    struct node *parent;
};

struct node *insert(node *, int);
void inorder_tree_walk(node *);