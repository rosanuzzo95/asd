#include <iostream>
#include <time.h>
#include "asd.cpp"

using namespace std;

int main()
{
    clock_t start = clock();

    struct node *root=NULL;

    //caso 1
    //root=insert(root, 12);

    //caso 2
    // root=insert(root, 12);
    // root=insert(root, 5);
    // root=insert(root, 18);
    // root=insert(root, 2);
    // root=insert(root, 9);
    // root=insert(root, 15);
    // root=insert(root, 19);
    // root=insert(root, 17);

    //caso 3
    // root=insert(root, 100);
    // root=insert(root, 1);
    // root=insert(root, 99);
    // root=insert(root, 2);
    // root=insert(root, 98);
    // root=insert(root, 3); 
    // root=insert(root, 97);
    // root=insert(root, 4);  

    //caso 4
    root=insert(root, 19);   
    root=insert(root, 12);
    root=insert(root, 5);
    root=insert(root, 18);
    root=insert(root, 2);
    root=insert(root, 9);
    root=insert(root, 15);
    root=insert(root, 17);     


    cout<<"tree:"<<endl;
    inorder_tree_walk(root);
    
    clock_t end = clock();
    cout<<"\ntempo totale in secondi: "<<(double)(end - start) / CLOCKS_PER_SEC;

    return 0;
}