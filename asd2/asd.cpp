#include <iostream>
#include "asd.h"

struct node *newNode(int key){
    node *newN=(node *)malloc(sizeof(node *));
    newN->key=key;
    newN->left=NULL;
    newN->right=NULL;

    return newN;
}

struct node *insert(node *root, int z){
    node *x=root;
    node *y=NULL;
    while(x!=NULL){
        y=x;
        if (z <= x->key){
            x=x->left;
        }
        else if (z > x->key){
            x=x->right;
        }
    }
    if (y == NULL){
        y=newNode(z);
    }
    else if (z <= y->key){
        y->left=newNode(z);
    }
    else if (z > y->key){
        y->right=newNode(z);
    }

    return y;
}

using namespace std;

int main()
{
    struct node *root=NULL;
    root=insert(root, 20);
    root=insert(root, 30);
    root=insert(root, 40);
    root=insert(root, 50);

    while(!root){
        cout<<"root: "<<root->key;
    }
    
    return 0;
}

