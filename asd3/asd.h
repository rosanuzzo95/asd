#include <iostream>

struct node {
    int key;
    struct node *right;
    struct node *left;
};

struct node *insert(node *, int);