#include <iostream>
#include <time.h>
#include "asd.cpp"

using namespace std;

int main()
{
    clock_t start = clock();

    struct node *root=NULL; 
    struct node *s=NULL;
    struct node *min=NULL;
    struct node *max=NULL;
    struct node *succ=NULL;
    struct node *pred=NULL;
    

    //caso 1
    //root=insert(root, 12);

    //caso 2
    // root=insert(root, 12);
    // root=insert(root, 5);
    // root=insert(root, 18);
    // root=insert(root, 2);
    // root=insert(root, 9);
    // root=insert(root, 15);
    // root=insert(root, 19);
    // root=insert(root, 17);

    //caso 3
    // root=insert(root, 100);
    // root=insert(root, 1);
    // root=insert(root, 99);
    // root=insert(root, 2);
    // root=insert(root, 98);
    // root=insert(root, 3); 
    // root=insert(root, 97);
    // root=insert(root, 4);  

    //caso 4
    root=tree_insert(root, 12);
    root=tree_insert(root, 19);   
    root=tree_insert(root, 5);
    root=tree_insert(root, 18);
    // root=tree_insert(root, 2);
    // root=tree_insert(root, 9);
    // root=tree_insert(root, 15);
    // root=tree_insert(root, 17);   


    cout<<"\ntree:"<<endl;
    inorder_tree_walk(root);

    s=tree_search(root, 12);
    cout<<" ricerca 12: "<<s<<"\n"<<endl;    

    s=tree_search(root, 19);
    cout<<" ricerca 19: "<<s<<"\n"<<endl;

    s=tree_search(root, 5);
    cout<<" ricerca 5: "<<s<<"\n"<<endl;

    s=tree_search(root, 18);
    cout<<" ricerca 18: "<<s<<"\n"<<endl;
    
    clock_t end = clock();
    cout<<"\ntempo totale in secondi: "<<(double)(end - start) / CLOCKS_PER_SEC;

    min=tree_minimun(root);
    cout<<"\nil nodo minimo e': "<<min->key<<" padre: "<<min->parent;

    max=tree_maximun(root);
    cout<<"\nil nodo massimo e': "<<max->key;

    succ=tree_successor(root);
    cout<<"\nil successore e': "<<succ->key;

    pred=tree_predecessor(root);
    cout<<"\nil successore e': "<<pred->key;


    return 0;
}