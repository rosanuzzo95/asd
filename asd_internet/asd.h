    struct node{
        int key;
        struct node *left;
        struct node *right;
    };

    struct node *TREE_INSERT(node *, int);