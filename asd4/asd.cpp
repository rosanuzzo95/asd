#include <iostream>
#include "asd.h"

using namespace std;

struct node *newNode(int key){
    node *newN=(node *)malloc(sizeof(node *));
    newN->key=key;
    newN->left=NULL;
    newN->right=NULL;

    return newN;
}

struct node *insert(node *root, int z){
    node *x=root;
    node *y=NULL;
    while(x!=NULL){
        y=x;
        if (z <= x->key){
            x=x->left;
        }
        else if (z > x->key){
            x=x->right;
        }
    }
    if (y == NULL){
        root=newNode(z);
    }
    else if (z <= y->key){
        y->left=newNode(z);
    }
    else if (z > y->key){
        y->right=newNode(z);
    }

    return root;
}

void inorder_tree_walk(node *x){
    if(x != NULL){
        inorder_tree_walk(x->left);
        cout<<x->key<<" ";
        inorder_tree_walk(x->right);
    }
}


