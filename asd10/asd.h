#include <iostream>

struct node {
    int key;
    node *right;
    node *left;
    node *parent;
};

struct node *tree_insert(node *, int);
void inorder_tree_walk(node *);
struct node *tree_search(node *, int);
struct node *tree_minimun(node *);
struct node *tree_maximun(node *);
struct node *tree_successor(node *);
struct node *tree_predecessor(node *);
struct node *tree_delete(node *, node *);
