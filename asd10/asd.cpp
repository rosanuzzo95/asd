#include <iostream>
#include "asd.h"

using namespace std;

struct node *newNode(int key){
    node *newN=(node *)malloc(sizeof(node *));
    newN->key=key;
    newN->left=NULL;
    newN->right=NULL;
    return newN;
}

struct node *tree_insert(node *root, int z){
    node *x=root;
    node *y=NULL;
    node *k=NULL;
    while(x!=NULL){
        y=x;
        if (z <= x->key){
            x=x->left;
        }
        else if (z > x->key){
            x=x->right;
        }
    }
    k=newNode(z);   
    k->parent=y;
    if (y == NULL){
        root=k;
    }
    else if (z < y->key){
        y->left=k;
    }
    else{
        y->right=k;
    }

    return root;
}

void inorder_tree_walk(node *x){
    if(x != NULL){
        inorder_tree_walk(x->left);
        if(x->parent==NULL)
            cout<<"padre: NIL nodo: "<<x->key<<" indirizzo: "<<x<<endl;
        else
            cout<<"padre: "<<(x->parent)->key<<" nodo: "<<x->key<<" indirizzo: "<<x<<endl;
        inorder_tree_walk(x->right);
    }
}

//ricerca più efficiente
struct node *tree_search(node *x, int k){
    while(x!=NULL && k!=x->key){
        if(k<x->key)
            x=x->left;
        else 
            x=x->right;
    }
    node *y=x->parent;
    //cout<<"padre: "<<y->key;
    return x;
}

struct node *tree_minimun(node *x){
    while (x->left!=NULL){
        x=x->left;
    }
    return x;
}

struct node *tree_maximun(node *x){
        while (x->right!=NULL){
        x=x->right;
    }
    return x;
}

struct node *tree_successor(node *x){
    node *y=NULL;
    if(x->right!=NULL)
        return tree_minimun(x->right);
    y=x->parent;
    while ((y != NULL) && (x==y->right)){
        x=y;
        y=y->parent;
    }
    return y;
}

struct node *tree_predecessor(node *x){
    node *y=NULL;
    if(x->left!=NULL)
        return tree_minimun(x->left);
    y=x->parent;
    while ((y != NULL) && (x==y->left)){
        x=y;
        y=y->parent;
    }
    return y;
}

//posso cercare il nodo tramite search ed eliminarlo
struct node *tree_delete(node *root, int n){
    node *z=tree_search(root, n);
    node *y=NULL;
    node *x=NULL;

    if (z->left == NULL || z->right == NULL)
        y=z;
    else
        y=tree_successor(z);
    if (y->left != NULL)
        x=y->left;
    else
        x=y->right;
    if (x != NULL)
        x->parent=y->parent;
    if (y->parent==NULL)
        root=x;
    else if(y==(y->parent)->left)
        (y->parent)->left=x;
    else
        (y->parent)->right=x;
    if (y!=z){
        z->key=y->key;
    }

return y;
}
